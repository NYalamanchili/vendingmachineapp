package com.machine.vending.model;

import java.util.Arrays;

public enum Status {
	DISPLAY_MENU(1), POWER_OFF(2), ITEM_IN_SELECTION(3), ACCEPT_COINS_STAGE_1(4), COMPLETE(5), STAND_BY(8), ACCEPT_COINS_STAGE_2(9);

	private int status;

	private Status(int status) {
		this.status = status;
	}

	public int status() {
		return this.status;
	}
	
	public static Status get(int status){
		return Arrays.stream(Status.values()).filter(vms -> vms.status == status).findFirst().orElse(null);
	}
}
