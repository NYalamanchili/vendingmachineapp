package com.machine.vending.model;

import java.util.Objects;

public class Item {

	private final String itemName;
	private final double price;

	public Item(final String itemName, final double price) {
		this.itemName = itemName;
		this.price = price;
	}

	public final String getItemName() {
		return itemName;
	}

	public final double getPrice() {
		return price;
	}
}
