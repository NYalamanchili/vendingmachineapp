package com.machine.vending.model;

public enum OrderStatus {

	ORDER_IN_PROGRESS("IN_PROGRESS"),ORDER_SUCCESSFUL("COMPLETE"), CANCEL_ORDER("CANCEL");
	private String status;
	OrderStatus(final String status){
		this.status = status;
	}
	
	public String getOrderStatus(){
		return this.status;
	}
}
