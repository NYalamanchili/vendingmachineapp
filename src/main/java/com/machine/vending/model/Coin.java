package com.machine.vending.model;

public class Coin {

	private final int value;
	private final int type;
	
	Coin(int value, int type){
		this.value = value;
		this.type = type;
	}

	public int getValue() {
		return value;
	}

	public int getType() {
		return type;
	}
}
