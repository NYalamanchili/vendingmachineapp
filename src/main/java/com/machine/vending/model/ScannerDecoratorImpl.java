package com.machine.vending.model;

import java.util.Scanner;

public class ScannerDecoratorImpl implements ScannerDecorator {

	final Scanner scanner;
	
	public ScannerDecoratorImpl(){
		scanner = new Scanner(System.in);
	}
	
	@Override
	public String next(){
		return scanner.next();
	}
	
	@Override
	public void close(){
		scanner.close();
	}
}
