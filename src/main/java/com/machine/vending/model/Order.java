package com.machine.vending.model;

public class Order {
	 private int total;
	 private final String itemName;
	 private final int itemPrice;
	 private OrderStatus status;
	 
	 public Order(final String itemName, final int itemPrice){
		 this.itemName = itemName;
		 this.itemPrice = itemPrice;
		 this.status = OrderStatus.ORDER_IN_PROGRESS;
	 }
	 
	 public void addAmount(int amount){
		 total = total +amount;
	 }
	 
	 public int getTotalAmount(){
		 return this.total;
	 }

	public int getItemPrice() {
		return itemPrice;
	}

	public String getItemName() {
		return itemName;
	}
	
	public OrderStatus getStatus(){
		return this.status;
	}
	
	public void setStatus(final OrderStatus status){
		this.status = status;
	}
}
