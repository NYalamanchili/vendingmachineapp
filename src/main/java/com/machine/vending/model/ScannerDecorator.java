package com.machine.vending.model;

public interface ScannerDecorator {

	void close();

	String next();

}
