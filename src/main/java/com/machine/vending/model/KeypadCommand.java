package com.machine.vending.model;

public enum KeypadCommand {
	ON("ON"), OFF("OFF"), CANCEL("CANCEL");

	private String cmd;

	KeypadCommand(String cmd) {
		this.cmd = cmd;
	}

	public String getCommand() {
		return this.cmd;
	}
}
