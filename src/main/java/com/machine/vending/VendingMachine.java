package com.machine.vending;

import com.machine.vending.model.KeypadCommand;
import com.machine.vending.model.Status;

public interface VendingMachine {

	Status getCurrentStatus();

	boolean isEnabled();

	void keyPadCommands(final KeypadCommand keypadCommand);

	void acceptInstructions(final String userInput);

}
