package com.machine.vending;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.machine.vending.config.VendingMachineConfig;
import com.machine.vending.model.KeypadCommand;
import com.machine.vending.model.ScannerDecorator;
import com.machine.vending.model.ScannerDecoratorImpl;

/**
 * Vending Machine App. ItemLoader loads the items and stock values. Initially
 * the stock for all items are set to 4. This main class instantiates the
 * VendingMachine takes the input from command prompt.
 *
 */
public class VendingMachineApp {
	private static final String SHUT_DOWN = "0";
	private static final List<KeypadCommand> KEYPAD_COMMANDS = Arrays.asList(KeypadCommand.ON, KeypadCommand.OFF,
			KeypadCommand.CANCEL);

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(VendingMachineConfig.class);
		VendingMachine vm = ctx.getBean(VendingMachineImpl.class);
		System.out.println("ON/on to Power ON");
		System.out.println("OFF/off to Power OFF");
		System.out.println("Shutdown machine:0");
		System.out.println("CANCEL/cancel to cancel order anytime");
		ScannerDecorator scannerDecorator = new ScannerDecoratorImpl();
		runVendingMachine(vm, scannerDecorator);
		scannerDecorator.close();
		((ConfigurableApplicationContext) ctx).close();
	}

	static void runVendingMachine(VendingMachine vm, ScannerDecorator scanner) {
		String userInput = null;
		while (!SHUT_DOWN.equals(userInput)) {
			userInput = scanner.next();
			Optional<KeypadCommand> keyPadCommand = VendingMachineApp.isKeyPadCommand(userInput);
			if (keyPadCommand.isPresent())
				vm.keyPadCommands(keyPadCommand.get());
			else
				vm.acceptInstructions(userInput);
		}
	}

	private static Optional<KeypadCommand> isKeyPadCommand(String userInput) {
		return KEYPAD_COMMANDS.stream().filter(command -> command.getCommand().equalsIgnoreCase(userInput)).findAny();
	}
}