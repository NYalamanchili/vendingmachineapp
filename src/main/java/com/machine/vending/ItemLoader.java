package com.machine.vending;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.machine.vending.dao.ItemCatalogueDao;
import com.machine.vending.model.Item;
/**
 * This is just used to load the data.
 *
 */
@Component
public class ItemLoader {
	
	@Autowired
	private ItemCatalogueDao itemCatalogueDao;
	
	
	public void loadItems() {
		itemCatalogueDao.clearItems();
		itemCatalogueDao.addItem(loadItem("A",0.60), 4);
		itemCatalogueDao.addItem(loadItem("B",1.00), 4);
		itemCatalogueDao.addItem(loadItem("C",1.70), 4);		
	}

	private Item loadItem(String itemName, double price) {
		return new Item(itemName, price);
		
	}
}
