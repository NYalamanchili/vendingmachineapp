package com.machine.vending.dao;

import java.util.List;
import java.util.Optional;

import com.machine.vending.dao.CoinTypeDaoImpl.CoinType;

public interface CoinTypeDao {

	void isCoinInStock(int amount, int coin);

	CoinType getCoinType(final int coin);

	List<Integer> getAcceptableCoins();

}
