package com.machine.vending.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class CoinTypeDaoImpl implements CoinTypeDao{
	
	public enum CoinType{
		PENCE,POUND,INVALID;
	}
	
	final List<Integer> acceptableCoins;
	
	CoinTypeDaoImpl(){
		acceptableCoins = Arrays.asList(10, 20, 50, 1);
	}
	
	@Override
	public List<Integer> getAcceptableCoins(){
		return acceptableCoins;
	}
	
	@Override
	public CoinType getCoinType(final int coin){
		final Optional<Integer> coinOpt = acceptableCoins.stream().filter(c -> c==coin).findFirst();
		return coinOpt.isPresent()?coinOpt.get() == 1 ? CoinType.POUND:CoinType.PENCE:CoinType.INVALID;
	}
	
	@Override
	public void isCoinInStock(int amount, int coin){
		//TODO check if coins are in stock
	}
}
