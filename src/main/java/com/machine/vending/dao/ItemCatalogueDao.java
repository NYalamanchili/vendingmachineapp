package com.machine.vending.dao;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import com.machine.vending.exception.StockUnavailableException;
import com.machine.vending.model.Item;

public interface ItemCatalogueDao {

	Collection<Item> getAllItems();

	Optional<Item> getItem(String itemName);

	void addItem(Item item, int quantity);

	void reduceQuantity(String itemName) throws StockUnavailableException;

	boolean isItemInStock(String itemName);

	void clearItems();


}
