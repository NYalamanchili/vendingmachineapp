package com.machine.vending.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.machine.vending.exception.StockUnavailableException;
import com.machine.vending.model.Item;

@Component
public class ItemCatalogueDaoImpl implements ItemCatalogueDao {

	private final List<Item> items;

	private final Map<String, Integer> itemsStock;

	public ItemCatalogueDaoImpl() {
		this.items = new ArrayList<Item>();
		this.itemsStock = new HashMap<String, Integer>();
	}

	@Override
	public void addItem(Item item, int quantity) {
		this.itemsStock.put(item.getItemName(), quantity);
		this.items.add(item);
	}

	@Override
	public Optional<Item> getItem(final String itemName) {
		return items.stream().filter(item -> item.getItemName().equalsIgnoreCase(itemName)).findFirst();
	}

	@Override
	public Collection<Item> getAllItems() {
		return Collections.unmodifiableCollection(items);
	}

	@Override
	public boolean isItemInStock(final String itemName) {
		final String normaliseditemName = itemName != null ? itemName.toUpperCase() : "";
		final int quantity = itemsStock.getOrDefault(normaliseditemName, 0);
		return quantity <= 0 ? false : true;
	}

	@Override
	public void reduceQuantity(final String itemName) throws StockUnavailableException {
		String normaliseditemName = itemName != null ? itemName.toUpperCase() : "";
		int quantity = itemsStock.getOrDefault(normaliseditemName, 0);
		if (quantity >= 1) {
			itemsStock.put(normaliseditemName, quantity-1);
		} else {
			throw new StockUnavailableException("No Stock");
		}
	}

	@Override
	public void clearItems() {
		items.clear();
	}
}
