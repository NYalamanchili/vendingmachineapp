package com.machine.vending;

import java.text.DecimalFormat;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.machine.vending.dao.CoinTypeDaoImpl.CoinType;
import com.machine.vending.exception.OrderNotFoundException;
import com.machine.vending.model.Item;
import com.machine.vending.model.KeypadCommand;
import com.machine.vending.model.Order;
import com.machine.vending.model.OrderStatus;
import com.machine.vending.model.Status;
import com.machine.vending.service.OrderService;
import com.machine.vending.service.VendingMachineService;

@Component
public class VendingMachineImpl  implements VendingMachine {

	@Autowired
	private VendingMachineService vendingMachineService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private ItemLoader itemLoader;
	private Status currentStatus;
	private boolean enabled;

	private void displayMenu() {
		vendingMachineService.itemsOnDisplay();
		currentStatus = Status.ITEM_IN_SELECTION;
	}

	private void itemInSelection(final String itemName) {
		final Optional<Item> itemByName = vendingMachineService.findItemByName(itemName);
		if (itemByName.isPresent()) {
			if (vendingMachineService.checkStock(itemName)) {
				final int price = (int) (itemByName.get().getPrice() * 100);
				orderService.createOrder(itemName, price);
				System.out.println("insert coins..");
				currentStatus = Status.ACCEPT_COINS_STAGE_1;
			} else {
				System.out.println("Item out of stock");
				displayMenu();
			}
		} else {
			System.out.println("Invalid input");
			displayMenu();
		}
	}

	private void acceptCoins(final String userInput) {
		if (vendingMachineService.isCoinAcceptable(userInput)) {
			int insertedCoin = Integer.parseInt(userInput);
			final CoinType coinType = vendingMachineService.getCoinType(insertedCoin);
			final Order order = orderService.getOrder();
			insertedCoin = (coinType == CoinType.POUND) ? insertedCoin * 100 : insertedCoin;
			order.addAmount(insertedCoin);
			final int difference = getBalance(order);
			if (difference < 0) {
				System.out.println(
						"'Cancel' to return coins or insert remaining " + formatValue((double) difference / 100));
				currentStatus = Status.ACCEPT_COINS_STAGE_1;
			} else
				processOrder();
		} else {
			System.out.println("Invalid coin type inserted..");
		}
	}

	private int getBalance(final Order order) {
		return order != null ? order.getTotalAmount() - order.getItemPrice() : 0;
	}

	@Override
	public void acceptInstructions(final String userInput) {
		if (isEnabled()) {
			switch (currentStatus) {
			case ITEM_IN_SELECTION:
				itemInSelection(userInput);
				break;
			case ACCEPT_COINS_STAGE_1:
				acceptCoins(userInput);
				break;
			case COMPLETE:
				processOrder();
				break;
			}
		}
	}

	@Override
	public void keyPadCommands(final KeypadCommand keypadCommand) {
		switch (keypadCommand) {
		case ON:
			currentStatus = Status.DISPLAY_MENU;
			enabled = true;
			itemLoader.loadItems();
			displayMenu();
			break;
		case OFF:
			enabled = false;
			System.out.println("Switched off");
			break;
		case CANCEL:
			cancelOrder();
		}
	}

	@Override
	public final boolean isEnabled() {
		return enabled;
	}

	@Override
	public final Status getCurrentStatus() {
		return currentStatus;
	}

	// Can be moved into util class
	private Double formatValue(double amount) {
		DecimalFormat df = new DecimalFormat("#.##");
		amount = Double.valueOf(df.format(amount));
		return amount;
	}

	// below Orders methods can be moved into a new orderProgressHandler class
	// which internally will call orderservice
	private void cancelOrder() {
		handleOrderStatus(OrderStatus.CANCEL_ORDER);
		final Order order = orderService.getOrder();
		System.out.println(
				"Order cancelled. Change returned =" + vendingMachineService.getChangeCoins(order.getTotalAmount()));
		removeOrderAndDisplayMenu();
	}

	private void processOrder() {
		handleOrderStatus(OrderStatus.ORDER_SUCCESSFUL);
		final Order order = orderService.getOrder();
		vendingMachineService.reduceStock(order.getItemName());
		System.out.println(
				"Order processed. Change returned =" + vendingMachineService.getChangeCoins(getBalance(order)));
		removeOrderAndDisplayMenu();
	}

	private void removeOrderAndDisplayMenu() {
		orderService.removeOrder();
		displayMenu();
	}

	private void handleOrderStatus(final OrderStatus orderStatus) {
		try {
			orderService.updateStatus(orderStatus);
		} catch (OrderNotFoundException onfe) {
			System.out.println(onfe.getMessage());
			displayMenu();
		}
	}
}