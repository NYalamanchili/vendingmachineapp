package com.machine.vending.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.machine.vending.dao.CoinTypeDaoImpl.CoinType;

public interface VendingMachineValidatonService {

	boolean isCoinAcceptable(final String coin);

	boolean isItemNameValid(final String itemName);
}
