package com.machine.vending.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.machine.vending.exception.OrderNotFoundException;
import com.machine.vending.model.Order;
import com.machine.vending.model.OrderStatus;

@Component
public class OrderServiceImpl implements OrderService {

	final private List<Order> orders;

	OrderServiceImpl() {
		orders = new ArrayList<Order>();
	}

	@Override
	public void createOrder(String itemName, int price) {
		Order order = new Order(itemName, price);
		orders.add(order);
	}

	@Override
	public Order getOrder() {
		return orders.size() > 0 ? orders.get(0) : null;
	}

	@Override
	public void removeOrder() {
		if (getOrder() != null) {
			// log details in the system
			orders.clear();
		}else {
			//throw custom exception OrderNotPresent
		}

	}

	@Override
	public boolean updateStatus(final OrderStatus status) throws OrderNotFoundException {
		Optional<Order> order = orders.stream().findAny().map(o -> {
			o.setStatus(status);
			return o;
		});
		if(!order.isPresent()){
			throw new OrderNotFoundException("Order not found");
		}
		return true;
	}
}
