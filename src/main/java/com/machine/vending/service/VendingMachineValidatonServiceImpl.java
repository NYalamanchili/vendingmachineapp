package com.machine.vending.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.machine.vending.dao.CoinTypeDao;
import com.machine.vending.dao.ItemCatalogueDao;

@Component
public class VendingMachineValidatonServiceImpl implements VendingMachineValidatonService {	
	@Autowired
	private ItemCatalogueDao itemCatalogueDao;
	@Autowired
	private CoinTypeDao coinTypeDao;

	@Override
	public boolean isItemNameValid(final String itemName) {
		// can throw custom exception from here need more time for that.
		return itemCatalogueDao.getItem(itemName).isPresent();
	}

	@Override
	public boolean isCoinAcceptable(final String coin) {
		return coinTypeDao.getAcceptableCoins().stream().filter(d -> String.valueOf(d).equalsIgnoreCase(coin)).findFirst().isPresent();
	}
}
