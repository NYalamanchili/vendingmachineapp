package com.machine.vending.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.machine.vending.dao.CoinTypeDaoImpl.CoinType;
import com.machine.vending.model.Item;

public interface VendingMachineService {

	boolean checkStock(String itemName);

	boolean isCoinAcceptable(String coinInserted);

	void reduceStock(String itemName);

	void itemsOnDisplay();

	Optional<Item> findItemByName(String itemName);

	CoinType getCoinType(int coin);

	List<Integer> getChangeCoins(final int amount);

}
