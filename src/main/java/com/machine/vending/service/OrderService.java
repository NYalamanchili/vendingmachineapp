package com.machine.vending.service;

import com.machine.vending.exception.OrderNotFoundException;
import com.machine.vending.model.Order;
import com.machine.vending.model.OrderStatus;

public interface OrderService {


	void createOrder(String itemName, int price);
	
	Order getOrder();

	void removeOrder();
	
	boolean updateStatus(final OrderStatus status) throws OrderNotFoundException;

}
