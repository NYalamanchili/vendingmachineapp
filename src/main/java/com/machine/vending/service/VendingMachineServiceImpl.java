package com.machine.vending.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.machine.vending.dao.ItemCatalogueDao;
import com.machine.vending.exception.StockUnavailableException;
import com.machine.vending.dao.CoinTypeDao;
import com.machine.vending.dao.CoinTypeDaoImpl.CoinType;
import com.machine.vending.model.Item;

@Component
public class VendingMachineServiceImpl implements VendingMachineService {

	@Autowired
	private VendingMachineValidatonService vendingMachineValidatonService;
	@Autowired
	private ItemCatalogueDao itemCatalogueDao;
	@Autowired
	private CoinTypeDao coinTypeDao;

	@Override
	public Optional<Item> findItemByName(final String itemName) {
		return vendingMachineValidatonService.isItemNameValid(itemName) ? itemCatalogueDao.getItem(itemName)
				: Optional.empty();
	}

	@Override
	public void itemsOnDisplay() {
		System.out.println("select an item:");
		itemCatalogueDao.getAllItems()
				.forEach(item -> System.out.println(item.getItemName() + " - £" + item.getPrice()));
	}

	@Override
	public void reduceStock(final String itemName) {
		try {
			itemCatalogueDao.reduceQuantity(itemName);
		} catch (StockUnavailableException sue) {
			System.out.println(sue.getMessage());
		}
	}

	@Override
	public boolean isCoinAcceptable(final String coinInserted) {
		return vendingMachineValidatonService.isCoinAcceptable(coinInserted);
	}

	@Override
	public boolean checkStock(final String itemName) {
		return itemCatalogueDao.isItemInStock(itemName);

	}

	@Override
	public CoinType getCoinType(int coin) {
		return coinTypeDao.getCoinType(coin);
	}

	@Override
	public List<Integer> getChangeCoins(final int amount) {
		List<Integer> change = null;
		Optional<Integer> optionalCoin = getCoin(amount);
		if (!optionalCoin.isPresent()) {
			change = findCoins(amount, coinTypeDao.getAcceptableCoins());
		} else {
			change = new ArrayList<Integer>();
			change.add(optionalCoin.get());
		}
		return change;
	}

	// This an be further simplified
	private List<Integer> findCoins(final int amount, List<Integer> acceptableCoins) {
		List<Integer> coins = new ArrayList<Integer>();
		int pence = 0;
		int total = 0;
		while (total < amount) {
			for (int coin : acceptableCoins) {
				CoinType coinType = coinTypeDao.getCoinType(coin);
				pence = coinType == coinType.POUND ? coin * 100 : coin;
				if (pence <= (amount - total)) {
					total = total + pence;
					coins.add(coin);
				}
			}
		}
		return coins;
	}

	private Optional<Integer> getCoin(int amount) {

		return coinTypeDao.getAcceptableCoins().stream().filter(c -> c == amount).findFirst();
	}
}
