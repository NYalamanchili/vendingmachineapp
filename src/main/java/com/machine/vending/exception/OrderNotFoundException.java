package com.machine.vending.exception;

public class OrderNotFoundException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4506727555337124335L;

	public OrderNotFoundException(String message){
		super(message);
	}
}
