package com.machine.vending.exception;

public class StockUnavailableException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4506727555337124335L;

	public StockUnavailableException(String message){
		super(message);
	}
}
