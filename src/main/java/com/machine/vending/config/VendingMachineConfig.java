package com.machine.vending.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages="com.machine.vending*")
public class VendingMachineConfig {	
}