package com.machine.vending.log;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class SystemConsoleInterceptor {
	
	public String getSysouts(Log applog) throws Exception {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(bos, true);
		PrintStream oldStream = System.out;
		System.setOut(printStream);
		try {
			applog.call();
		} finally {
			System.setOut(oldStream);
		}
		return bos.toString();
	}
}