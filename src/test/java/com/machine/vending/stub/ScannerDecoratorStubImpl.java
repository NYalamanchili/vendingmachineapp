package com.machine.vending.stub;

import java.util.ArrayList;
import java.util.List;

import com.machine.vending.model.ScannerDecorator;

public class ScannerDecoratorStubImpl implements ScannerDecorator {

	private final List<String> scannerInputCommands;
	private int scannerInputCount;

	public ScannerDecoratorStubImpl() {
		scannerInputCommands = new ArrayList<>();
		scannerInputCount = 0;
	}

	public String next() {
		return scannerInputCommands.get(scannerInputCount++);
	}

	public void close() {
	}

	public void addScannerInput(String inputCommand) {
		scannerInputCommands.add(inputCommand);
	}
	
	public void addScannerInputCommands(List<String> inputCommands) {
		scannerInputCommands.addAll(inputCommands);
	}
}
