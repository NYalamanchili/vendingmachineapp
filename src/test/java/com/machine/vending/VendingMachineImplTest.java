package com.machine.vending;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.machine.vending.ItemLoader;
import com.machine.vending.dao.CoinTypeDaoImpl.CoinType;
import com.machine.vending.model.Item;
import com.machine.vending.model.KeypadCommand;
import com.machine.vending.model.Order;
import com.machine.vending.model.Status;
import com.machine.vending.service.OrderService;
import com.machine.vending.service.VendingMachineService;

@RunWith(MockitoJUnitRunner.class)
public class VendingMachineImplTest {

	@Mock
	private VendingMachineService vendingMachineService;

	@Mock
	private OrderService orderService;
	
	@Mock
	private ItemLoader itemLoader;
	
	@InjectMocks
	private VendingMachineImpl vendingMachine;

	private String userInput;
	
	@Test
	public void testKeypadCommandsOn(){
		Mockito.doNothing().when(itemLoader).loadItems();
		Mockito.doNothing().when(vendingMachineService).itemsOnDisplay();
		vendingMachine.keyPadCommands(KeypadCommand.ON);
		Mockito.verify(itemLoader).loadItems();
		Mockito.verify(vendingMachineService).itemsOnDisplay();
		assertTrue(vendingMachine.getCurrentStatus()== Status.ITEM_IN_SELECTION);
		assertTrue(vendingMachine.isEnabled());
	}
	
	@Test
	public void testKeypadCommandsOff(){
		Mockito.doNothing().when(vendingMachineService).itemsOnDisplay();
		vendingMachine.keyPadCommands(KeypadCommand.OFF);
		assertFalse(vendingMachine.isEnabled());
		
	}
	
	@Test
	public void testAcceptInstuctionsAcceptCoinsStageOneStatus(){
		ReflectionTestUtils.setField(vendingMachine, "currentStatus", Status.ITEM_IN_SELECTION);
		ReflectionTestUtils.setField(vendingMachine, "enabled", true);
		userInput = "MockItem" ;
		final Item item = new Item("MockItem", 1.20);
		Optional<Item> itemByName = Optional.of(item);
		Mockito.when(vendingMachineService.findItemByName(userInput)).thenReturn(itemByName);
		Mockito.when(vendingMachineService.checkStock("MockItem")).thenReturn(true);
		Mockito.doNothing().when(orderService).createOrder("MockItem",120);
		
		vendingMachine.acceptInstructions(userInput);
		
		Mockito.verify(vendingMachineService).findItemByName("MockItem");
		Mockito.verify(vendingMachineService).checkStock("MockItem");
		Mockito.verify(orderService).createOrder("MockItem",120);
		assertTrue(vendingMachine.getCurrentStatus()== Status.ACCEPT_COINS_STAGE_1);
	}
	
	@Test
	public void testAcceptInstuctionsItemInSelectionNotPresent(){
		ReflectionTestUtils.setField(vendingMachine, "currentStatus", Status.ITEM_IN_SELECTION);
		ReflectionTestUtils.setField(vendingMachine, "enabled", true);
		userInput = "unknownItem" ;
		Optional<Item> itemByName = Optional.empty();
		Mockito.when(vendingMachineService.findItemByName(userInput)).thenReturn(itemByName);
		
		vendingMachine.acceptInstructions(userInput);
		
		Mockito.verify(vendingMachineService, Mockito.times(1)).findItemByName("unknownItem");
		Mockito.verify(vendingMachineService, Mockito.times(0)).checkStock("unknownItem");
		Mockito.verify(orderService, Mockito.times(0)).createOrder("unknonwItem",120);
		assertTrue(vendingMachine.getCurrentStatus()== Status.ITEM_IN_SELECTION);
	}
	
	@Test
	public void testAcceptInstuctionsItemOutOfStock(){
		ReflectionTestUtils.setField(vendingMachine, "currentStatus", Status.ITEM_IN_SELECTION);
		ReflectionTestUtils.setField(vendingMachine, "enabled", true);
		userInput = "OutOfStockItem" ;
		final Item item = new Item("OutOfStockItem", 1.20);
		Optional<Item> itemByName = Optional.of(item);
		Mockito.when(vendingMachineService.findItemByName(userInput)).thenReturn(itemByName);
		Mockito.when(vendingMachineService.checkStock("OutOfStockItem")).thenReturn(false);
		
		vendingMachine.acceptInstructions(userInput);
		
		Mockito.verify(vendingMachineService).findItemByName("OutOfStockItem");
		Mockito.verify(vendingMachineService).checkStock("OutOfStockItem");
		Mockito.verify(orderService, Mockito.times(0)).createOrder("OutOfStockItem",120);
		assertTrue(vendingMachine.getCurrentStatus()== Status.ITEM_IN_SELECTION);
	}

	@Test
	public void testAcceptInstuctionsAcceptCoinsStageOne(){
		ReflectionTestUtils.setField(vendingMachine, "currentStatus", Status.ACCEPT_COINS_STAGE_1);
		ReflectionTestUtils.setField(vendingMachine, "enabled", true);
		userInput = "20";
		final Order order = new Order("MockItem", 50);
		Mockito.when(vendingMachineService.isCoinAcceptable(userInput)).thenReturn(true);
		Mockito.when(vendingMachineService.getCoinType(20)).thenReturn(CoinType.PENCE);
		Mockito.when(orderService.getOrder()).thenReturn(order);
		
		vendingMachine.acceptInstructions(userInput);
		
		Mockito.verify(vendingMachineService).isCoinAcceptable("20");
		Mockito.verify(vendingMachineService).getCoinType(20);
		Mockito.verify(orderService).getOrder();
		assertTrue(vendingMachine.getCurrentStatus()== Status.ACCEPT_COINS_STAGE_1);
	}
	
	@Test
	public void testAcceptInstuctionsAcceptCoinsStageOneOrderSuccessful(){
		ReflectionTestUtils.setField(vendingMachine, "currentStatus", Status.ACCEPT_COINS_STAGE_1);
		ReflectionTestUtils.setField(vendingMachine, "enabled", true);
		userInput = "50";
		final Order order = new Order("MockItem", 50);
		Mockito.when(vendingMachineService.isCoinAcceptable(userInput)).thenReturn(true);
		Mockito.when(vendingMachineService.getCoinType(50)).thenReturn(CoinType.PENCE);
		Mockito.when(orderService.getOrder()).thenReturn(order);
		Mockito.doNothing().when(orderService).removeOrder();
		
		vendingMachine.acceptInstructions(userInput);
		
		Mockito.verify(vendingMachineService).isCoinAcceptable("50");
		Mockito.verify(vendingMachineService).getCoinType(50);
		ArgumentCaptor<String> orderStatusCaptor = ArgumentCaptor.forClass(String.class);
		Mockito.verify(orderService, Mockito.times(2)).getOrder();
		Mockito.verify(orderService, Mockito.times(1)).removeOrder();
		assertTrue(vendingMachine.getCurrentStatus()== Status.ITEM_IN_SELECTION);
	}
	
	@Test
	public void testAcceptInstuctionsAcceptCoinsStageOneInvalidCoin(){
		ReflectionTestUtils.setField(vendingMachine, "currentStatus", Status.ACCEPT_COINS_STAGE_1);
		ReflectionTestUtils.setField(vendingMachine, "enabled", true);
		userInput = "5";
		Mockito.when(vendingMachineService.isCoinAcceptable(userInput)).thenReturn(false);
		
		vendingMachine.acceptInstructions(userInput);
		
		Mockito.verify(vendingMachineService).isCoinAcceptable("5");
		Mockito.verifyZeroInteractions(vendingMachineService);
		Mockito.verifyZeroInteractions(orderService);
		assertTrue(vendingMachine.getCurrentStatus()== Status.ACCEPT_COINS_STAGE_1);
	}
}
