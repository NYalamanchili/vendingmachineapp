package com.machine.vending.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.machine.vending.VendingMachineImpl;
import com.machine.vending.exception.StockUnavailableException;
import com.machine.vending.model.Item;

@RunWith(MockitoJUnitRunner.class)
public class ItemCatalogueDaoImplTest {
	
	private ItemCatalogueDaoImpl itemCatalogueDaoImpl;
	private List<Item> items;
	private Map<String, Integer> itemsStock;

	@Before
	public void setup() {
		itemCatalogueDaoImpl = new ItemCatalogueDaoImpl();
		itemsStock = createItemsStock();
		items = createItems();
		ReflectionTestUtils.setField(itemCatalogueDaoImpl, "itemsStock", itemsStock);
		ReflectionTestUtils.setField(itemCatalogueDaoImpl, "items", items);
	}
	
	@Test
	public void testReduceQuantity() throws StockUnavailableException{
		itemCatalogueDaoImpl.reduceQuantity("A");
		assertTrue(itemsStock.get("A") == 3);
	}
	
	@Test(expected=StockUnavailableException.class)
	public void testReduceQuantityThrowsStockUnavailable() throws StockUnavailableException{
		itemsStock.put("A", 0);
		itemCatalogueDaoImpl.reduceQuantity("A");
		assertTrue(itemsStock.get("A") == 3);
	}
	
	@Test
	public void testIsItemInStock(){
		assertTrue(itemCatalogueDaoImpl.isItemInStock("A"));
	}
	
	@Test
	public void testIsItemInStockUnavailable(){
		itemsStock.put("A", 0);
		assertFalse(itemCatalogueDaoImpl.isItemInStock("A"));
	}
	
	@Test
	public void testGetAllItems(){
		assertEquals(items.size(),itemCatalogueDaoImpl.getAllItems().size());
	}
	
	@Test
	public void testGetItemPresent(){
		Optional<Item> item = itemCatalogueDaoImpl.getItem("B");
		assertTrue(item.isPresent());
	}
	
	@Test
	public void getItemNotPresent(){
		Optional<Item> item = itemCatalogueDaoImpl.getItem("D");
		assertFalse(item.isPresent());
	}
	
	@Test
	public void testAddItem(){
		itemCatalogueDaoImpl.addItem(new Item("D", .80), 2);
		assertTrue(itemsStock.get("D")==2);
		assertTrue(items.stream().map(Item::getItemName).filter(name -> "D".equalsIgnoreCase(name)).findAny().isPresent());
	}
	private Map<String, Integer> createItemsStock(){
		itemsStock = new HashMap<>();
		itemsStock.put("A", 4);
		itemsStock.put("B", 4);
		itemsStock.put("C", 4);
		return itemsStock;
	}
	
	private List<Item> createItems(){
		//need a modifiable list
		return items = new ArrayList<>(Arrays.asList(new Item("A", 0.60), new Item("B", 1.00), new Item("C", 1.70)));
	}
}
