package com.machine.vending;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.machine.vending.VendingMachineImpl;
import com.machine.vending.VendingMachineApp;
import com.machine.vending.config.VendingMachineConfig;
import com.machine.vending.log.SystemConsoleInterceptor;
import com.machine.vending.stub.ScannerDecoratorStubImpl;

import junit.framework.Assert;

public class VendingMachineAppIntegrationTest {

	private static final String PATH = "integration-test/expectedOutput/";
	private VendingMachine vm;
	private ScannerDecoratorStubImpl scanner;
	private SystemConsoleInterceptor consoleInterceptor;

	@Before
	public void setUp() {
		System.out.println("*****************Case Starts *************************");
		ApplicationContext ctx = new AnnotationConfigApplicationContext(VendingMachineConfig.class);
		vm = ctx.getBean(VendingMachineImpl.class);
		scanner = new ScannerDecoratorStubImpl();
		consoleInterceptor = new SystemConsoleInterceptor();
	}

	@After
	public void end() {
		System.out.println("*****************Case Ends ***************************");
	}

	@Test
	public void testVendingMachineBuyItemA() throws Exception {
		Path path = Paths.get(getClass().getClassLoader().getResource(PATH + "buyItemA.txt").toURI());
		scanner.addScannerInputCommands(Arrays.asList("ON", "A", "20", "20", "50", "OFF", "0"));
		VendingMachineApp.runVendingMachine(vm, scanner);

	}

	@Test
	public void testVendingMachineBuyItemB() throws Exception {
		Path path = Paths.get(getClass().getClassLoader().getResource(PATH + "buyItemB.txt").toURI());
		scanner.addScannerInputCommands(Arrays.asList("ON", "B", "50", "20", "20", "50", "OFF", "0"));

		String appLog = consoleInterceptor.getSysouts(() -> VendingMachineApp.runVendingMachine(vm, scanner));

		System.out.println(appLog);

		compare(path, Arrays.asList(appLog.split("\\n")));

	}

	@Test
	public void testVendingMachineBuyItemC() throws Exception {
		Path path = Paths.get(getClass().getClassLoader().getResource(PATH + "buyItemC.txt").toURI());
		scanner.addScannerInputCommands(Arrays.asList("ON", "C", "50", "20", "50", "50", "OFF", "0"));

		String appLog = consoleInterceptor.getSysouts(() -> VendingMachineApp.runVendingMachine(vm, scanner));

		System.out.println(appLog);

		compare(path, Arrays.asList(appLog.split("\\n")));
	}

	@Test
	public void testVendingMachineOutOfStock() throws Exception {
		scanner.addScannerInputCommands(Arrays.asList("ON", "A", "20", "20", "20", "A", "20", "20", "20", "A", "20",
				"20", "20", "A", "20", "20", "20", "A", "OFF", "0"));
		String appLog = consoleInterceptor.getSysouts(() -> VendingMachineApp.runVendingMachine(vm, scanner));

		System.out.println(appLog);

		assertTrue(Arrays.stream(appLog.split("\\n")).filter(s -> "Item out of stock".equals(s)).findAny().isPresent());
	}

	@Test
	public void testVendingMachineInvalidCoinInput() throws Exception {
		Path path = Paths.get(getClass().getClassLoader().getResource(PATH + "invalidCoinInput.txt").toURI());
		scanner.addScannerInputCommands(Arrays.asList("ON", "A", "5", "OFF", "0"));

		String appLog = consoleInterceptor.getSysouts(() -> VendingMachineApp.runVendingMachine(vm, scanner));

		System.out.println(appLog);

		compare(path, Arrays.asList(appLog.split("\\n")));
	}

	private void compare(final Path resourcePath, final List<String> actual) {
		Stream<String> lines = null;
		try {
			lines = Files.lines(resourcePath);
			lines.forEach(line -> {
				if (!actual.contains(line)) {
					Assert.fail("expected:" + line);
				}
			});
		} catch (IOException ioe) {
			Assert.fail();
		} finally {
			lines.close();
		}
	}
}
