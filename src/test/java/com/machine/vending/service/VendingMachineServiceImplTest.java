package com.machine.vending.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.machine.vending.dao.CoinTypeDao;
import com.machine.vending.dao.CoinTypeDaoImpl.CoinType;
import com.machine.vending.dao.ItemCatalogueDao;
import com.machine.vending.exception.StockUnavailableException;
import com.machine.vending.model.Item;

@RunWith(MockitoJUnitRunner.class)
public class VendingMachineServiceImplTest {

	@Mock
	private VendingMachineValidatonService vendingMachineValidatonService;
	@Mock
	private ItemCatalogueDao itemCatalogueDao;
	@Mock
	private CoinTypeDao coinTypeDao;

	@InjectMocks
	private VendingMachineServiceImpl vendingMachineService;

	@Test
	public void testFindItemByName() {
		String itemName = "MockItem";

		Mockito.when(vendingMachineValidatonService.isItemNameValid(itemName)).thenReturn(true);
		Mockito.when(itemCatalogueDao.getItem(itemName)).thenReturn(Optional.of(new Item("MockItem", 1)));

		Optional<Item> findItemByName = vendingMachineService.findItemByName(itemName);

		Mockito.verify(vendingMachineValidatonService).isItemNameValid("MockItem");
		Mockito.verify(itemCatalogueDao).getItem("MockItem");
		assertEquals("MockItem", findItemByName.get().getItemName());

	}

	@Test
	public void testFindItemByNameNotPresent() {
		String itemName = "MockItem";
		Mockito.when(vendingMachineValidatonService.isItemNameValid(itemName)).thenReturn(false);

		Optional<Item> findItemByName = vendingMachineService.findItemByName(itemName);

		Mockito.verify(vendingMachineValidatonService).isItemNameValid("MockItem");
		assertFalse(findItemByName.isPresent());

	}

	@Test
	public void testItemsOnDisplay() {
		Mockito.when(itemCatalogueDao.getAllItems())
				.thenReturn(Arrays.asList(new Item("MockItem1", 1), new Item("MockItem2", 0.50)));
		vendingMachineService.itemsOnDisplay();
		Mockito.verify(itemCatalogueDao).getAllItems();
	}

	@Test
	public void testGetChangeCoinsWithAmountAsDenomination() {
		Mockito.when(coinTypeDao.getAcceptableCoins()).thenReturn(Arrays.asList(10, 20, 50, 1));
		Mockito.when(coinTypeDao.getCoinType(Mockito.anyInt())).thenAnswer(ans -> {
			return (int) ans.getArguments()[0]  == 1 ? CoinType.POUND : CoinType.PENCE;
		});

		List<Integer> changeCoins = vendingMachineService.getChangeCoins(50);

		Mockito.verify(coinTypeDao, Mockito.times(1)).getAcceptableCoins();
		ArgumentCaptor<Integer> coinTypeCaptor = ArgumentCaptor.forClass(Integer.class);
		Mockito.verify(coinTypeDao, Mockito.times(0)).getCoinType(coinTypeCaptor.capture());
		assertEquals(50, changeCoins.stream().reduce(0, (a, b) -> a + b).intValue());
	}
	
	@Test
	public void testGetChangeCoinsAmountAsNoDenomination() {
		Mockito.when(coinTypeDao.getAcceptableCoins()).thenReturn(Arrays.asList(10, 20, 50, 1));
		Mockito.when(coinTypeDao.getCoinType(Mockito.anyInt())).thenAnswer(ans -> {
			return (int) ans.getArguments()[0]  == 1 ? CoinType.POUND : CoinType.PENCE;
		});

		List<Integer> changeCoins = vendingMachineService.getChangeCoins(60);

		Mockito.verify(coinTypeDao, Mockito.times(2)).getAcceptableCoins();
		ArgumentCaptor<Integer> coinTypeCaptor = ArgumentCaptor.forClass(Integer.class);
		Mockito.verify(coinTypeDao, Mockito.times(8)).getCoinType(coinTypeCaptor.capture());
		assertEquals(60, changeCoins.stream().reduce(0, (a, b) -> a + b).intValue());
	}

	@Test
	public void testIsCoinAcceptableValid() {
		Mockito.when(vendingMachineValidatonService.isCoinAcceptable("50")).thenReturn(true);
		boolean coinAcceptable = vendingMachineValidatonService.isCoinAcceptable("50");
		ArgumentCaptor<String> coinCapture = ArgumentCaptor.forClass(String.class);
		Mockito.verify(vendingMachineValidatonService).isCoinAcceptable(coinCapture.capture());
		assertTrue(coinAcceptable);
		assertEquals("50", coinCapture.getValue());
	}
	
	@Test
	public void testIsCoinAcceptableInValid() {
		Mockito.when(vendingMachineValidatonService.isCoinAcceptable("40")).thenReturn(false);
		boolean coinAcceptable = vendingMachineService.isCoinAcceptable("40");
		ArgumentCaptor<String> coinCapture = ArgumentCaptor.forClass(String.class);
		Mockito.verify(vendingMachineValidatonService).isCoinAcceptable(coinCapture.capture());
		assertFalse(coinAcceptable);
		assertEquals("40", coinCapture.getValue());
	}

	@Test
	public void testCheckStockValidItemUpperCase() {
		Mockito.when(itemCatalogueDao.isItemInStock("A")).thenReturn(true);
		boolean itemInStock = vendingMachineService.checkStock("A");
		ArgumentCaptor<String> itemNameCaptor = ArgumentCaptor.forClass(String.class);
		Mockito.verify(itemCatalogueDao).isItemInStock(itemNameCaptor.capture());
		assertTrue(itemInStock);
		assertEquals("A", itemNameCaptor.getValue());

	}
	
	@Test
	public void testCheckStockValidItemLowerCase() {
		Mockito.when(itemCatalogueDao.isItemInStock("a")).thenReturn(true);
		boolean itemInStock = vendingMachineService.checkStock("a");
		ArgumentCaptor<String> itemNameCaptor = ArgumentCaptor.forClass(String.class);
		Mockito.verify(itemCatalogueDao).isItemInStock(itemNameCaptor.capture());
		assertTrue(itemInStock);
		assertEquals("a", itemNameCaptor.getValue());

	}
	
	@Test
	public void testCheckStockInValidItem() {
		Mockito.when(itemCatalogueDao.isItemInStock("D")).thenReturn(false);
		boolean itemInStock = vendingMachineService.checkStock("D");
		ArgumentCaptor<String> itemNameCaptor = ArgumentCaptor.forClass(String.class);
		Mockito.verify(itemCatalogueDao).isItemInStock(itemNameCaptor.capture());
		assertFalse(itemInStock);
		assertEquals("D", itemNameCaptor.getValue());

	}

	@Test
	public void testGetCoinTypePound() {
		Mockito.when(coinTypeDao.getCoinType(Mockito.anyInt())).thenAnswer(ans -> {
			return (int) ans.getArguments()[0] == 1 ? CoinType.POUND:CoinType.PENCE;
		});
		CoinType coinType = vendingMachineService.getCoinType(1);
		Mockito.verify(coinTypeDao).getCoinType(1);
		assertTrue(coinType == CoinType.POUND);
	}
	@Test
	public void testGetCoinTypePence() {
		Mockito.when(coinTypeDao.getCoinType(Mockito.anyInt())).thenAnswer(ans -> {
			return (int) ans.getArguments()[0] == 1 ? CoinType.POUND:CoinType.PENCE;
		});
		CoinType coinType = vendingMachineService.getCoinType(20);
		Mockito.verify(coinTypeDao).getCoinType(20);
		assertTrue(coinType == CoinType.PENCE);
	}
	
	@Test
	public void testReduceStock() throws StockUnavailableException {
		Mockito.doNothing().when(itemCatalogueDao).reduceQuantity("A");
		vendingMachineService.reduceStock("A");
		Mockito.verify(itemCatalogueDao).reduceQuantity("A");
	}
}
