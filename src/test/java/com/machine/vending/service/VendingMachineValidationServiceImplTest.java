package com.machine.vending.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.machine.vending.dao.CoinTypeDao;
import com.machine.vending.dao.ItemCatalogueDao;
import com.machine.vending.model.Item;


@RunWith(MockitoJUnitRunner.class)
public class VendingMachineValidationServiceImplTest {

	@Mock
	private ItemCatalogueDao itemCatalogueDao;
	@Mock
	private CoinTypeDao coinTypeDao;

	@InjectMocks
	private VendingMachineValidatonServiceImpl vendingMachineValidatonServiceImpl;
	
	@Test
	public void testIsItemNameValid() {
		Mockito.when(itemCatalogueDao.getItem("A")).thenReturn(Optional.of(new Item("A", 0.6)));
		boolean itemNameValid = vendingMachineValidatonServiceImpl.isItemNameValid("A");
		assertTrue(itemNameValid);
		Mockito.verify(itemCatalogueDao).getItem("A");
	}
	
	@Test
	public void testIsItemNameInvalid() {
		Mockito.when(itemCatalogueDao.getItem("A")).thenReturn(Optional.empty());
		boolean itemNameValid = vendingMachineValidatonServiceImpl.isItemNameValid("A");
		assertFalse(itemNameValid);
		Mockito.verify(itemCatalogueDao).getItem("A");
	}

	@Test
	public void testIsCoinAcceptableValid() {
		Mockito.when(coinTypeDao.getAcceptableCoins()).thenReturn(Arrays.asList(10,20,50,1));
		boolean coinAcceptable = vendingMachineValidatonServiceImpl.isCoinAcceptable("20");
		assertTrue(coinAcceptable);
		Mockito.verify(coinTypeDao).getAcceptableCoins();
	}
	
	@Test
	public void testIsCoinAcceptableInvalid() {
		Mockito.when(coinTypeDao.getAcceptableCoins()).thenReturn(Arrays.asList(10,50,1));
		boolean coinAcceptable = vendingMachineValidatonServiceImpl.isCoinAcceptable("20");
		assertFalse(coinAcceptable);
		Mockito.verify(coinTypeDao).getAcceptableCoins();
	}
}
