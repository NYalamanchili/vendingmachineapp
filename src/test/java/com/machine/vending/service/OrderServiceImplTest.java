package com.machine.vending.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import javax.security.auth.login.FailedLoginException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.machine.vending.model.Order;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
	private OrderServiceImpl orderServiceImpl;

	@Before
	public void setup() {
		orderServiceImpl = new OrderServiceImpl();
	}

	@Test
	public void createOrder() {
		orderServiceImpl.createOrder("A", 60);
		assertEquals("A", orderServiceImpl.getOrder().getItemName());
		assertEquals(60, orderServiceImpl.getOrder().getItemPrice());
	}

	@Test
	public void removeOrder() {
		orderServiceImpl.createOrder("A", 60);
		try {
			orderServiceImpl.removeOrder();
		} catch (Exception ex) {
			fail();
		}
	}

	@Test
	public void removeOrderWithNoOrderPresent() {
		try {
			orderServiceImpl.removeOrder();
		} catch (Exception ex) {
			fail();
		}
	}
}
